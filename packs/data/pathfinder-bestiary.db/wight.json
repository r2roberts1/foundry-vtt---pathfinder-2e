{
    "_id": "ZPjQkKVMi3xoPcU0",
    "data": {
        "abilities": {
            "cha": {
                "mod": 2
            },
            "con": {
                "mod": 4
            },
            "dex": {
                "mod": 1
            },
            "int": {
                "mod": 0
            },
            "str": {
                "mod": 4
            },
            "wis": {
                "mod": 3
            }
        },
        "attributes": {
            "ac": {
                "details": "",
                "value": 18
            },
            "allSaves": {
                "value": ""
            },
            "hp": {
                "details": "negative healing",
                "max": 50,
                "temp": 0,
                "value": 50
            },
            "initiative": {
                "ability": "perception"
            },
            "perception": {
                "value": 10
            },
            "shield": {
                "ac": 0,
                "brokenThreshold": 0,
                "hardness": 0,
                "max": 0,
                "value": 0
            },
            "speed": {
                "otherSpeeds": [],
                "value": "25"
            }
        },
        "details": {
            "alignment": {
                "value": "LE"
            },
            "creatureType": "Undead",
            "flavorText": "<p>Wights are undead humanoids that, much like wraiths, can drain the life from living creatures with but a touch. They arise as a result of necromantic rituals, especially violent deaths, or the sheer malevolent will of the deceased.</p>\n<p>As many types of wights exist as types of people from which they might be created. Hulking brutes, skittering sneaks, and cunning tinkers all make for different wights with different niches to fill. Environment, too, plays a part in determining a wight's special abilities and defenses. Frost wights, for instances, can be found in the parts of the world where exposure is a common end. Regardless, wights typically haunt burial grounds, catacombs, or other places of the dead. But their hunger is targeted toward the living-those individuals who remind them of the shackles of mortality and whom they feel compelled to \"free\" to the state of undeath.</p>\n<p>A single wight can wreak a lot of havoc if it is compelled to rise from its tomb. Because creatures slain by wights become wights as well, all it takes is a single wight and a handful of unlucky graveyard visitors to create a veritable horde of these undead. Thus, canny priests and adventurers know that the best solution to a wight problem is swift and total eradication. Care must be taken, though, to destroy wight spawn before attempting to destroy the parent wight, for spawn without a master gain the ability to create spawn of their own.</p>\n<p>Durable and sustained as they are by negative energy, wights can last in harsh environments without decaying the way some lesser undead do. They might dwell in high mountain passes, sealed passageways, or submerged in bogs or lakes for decades or even centuries before the passage of an unsuspecting traveler rouses them from their rest.</p>",
            "level": {
                "value": 3
            },
            "source": {
                "value": ""
            }
        },
        "resources": {},
        "saves": {
            "fortitude": {
                "saveDetail": "",
                "value": 11
            },
            "reflex": {
                "saveDetail": "",
                "value": 6
            },
            "will": {
                "saveDetail": "",
                "value": 10
            }
        },
        "traits": {
            "attitude": {
                "value": "hostile"
            },
            "ci": [],
            "di": {
                "custom": "",
                "value": [
                    "death-effects",
                    "disease",
                    "paralyzed",
                    "poison",
                    "unconscious"
                ]
            },
            "dr": [],
            "dv": [],
            "languages": {
                "custom": "",
                "selected": [],
                "value": [
                    "common",
                    "necril"
                ]
            },
            "rarity": {
                "value": "common"
            },
            "senses": {
                "value": "darkvision"
            },
            "size": {
                "value": "med"
            },
            "traits": {
                "custom": "",
                "value": [
                    "undead",
                    "wight"
                ]
            }
        }
    },
    "effects": [],
    "flags": {},
    "img": "systems/pf2e/icons/default-icons/npc.svg",
    "items": [
        {
            "_id": "u9tY2pkbZxNzmxoe",
            "data": {
                "attack": {
                    "value": ""
                },
                "attackEffects": {
                    "custom": "",
                    "value": [
                        "drain-life"
                    ]
                },
                "bonus": {
                    "value": 12
                },
                "damageRolls": {
                    "xhnoig9jfy8quml6d0n7": {
                        "damage": "1d6+4",
                        "damageType": "slashing"
                    }
                },
                "description": {
                    "value": ""
                },
                "rules": [],
                "slug": null,
                "source": {
                    "value": ""
                },
                "traits": {
                    "custom": "",
                    "rarity": {
                        "value": "common"
                    },
                    "value": []
                },
                "weaponType": {
                    "value": "melee"
                }
            },
            "effects": [],
            "flags": {},
            "img": "systems/pf2e/icons/default-icons/melee.svg",
            "name": "Claw",
            "sort": 100000,
            "type": "melee"
        },
        {
            "_id": "esy7Ob0vQqHjpefw",
            "data": {
                "actionCategory": {
                    "value": "interaction"
                },
                "actionType": {
                    "value": "passive"
                },
                "actions": {
                    "value": null
                },
                "description": {
                    "value": "<p>A monster with darkvision can see perfectly well in areas of darkness and dim light, though such vision is in black and white only. Some forms of magical darkness, such as a 4th-level @Compendium[pf2e.spells-srd.Darkness]{Darkness} spell, block normal darkvision. A monster with @Compendium[pf2e.bestiary-ability-glossary-srd.Greater Darkvision]{Greater Darkvision}, however, can see through even these forms of magical darkness.</p>"
                },
                "requirements": {
                    "value": ""
                },
                "rules": [],
                "slug": "darkvision",
                "source": {
                    "value": ""
                },
                "traits": {
                    "custom": "",
                    "rarity": {
                        "value": "common"
                    },
                    "value": []
                },
                "trigger": {
                    "value": ""
                },
                "weapon": {
                    "value": ""
                }
            },
            "effects": [],
            "flags": {
                "core": {
                    "sourceId": "Compendium.pf2e.bestiary-ability-glossary-srd.qCCLZhnp2HhP3Ex6"
                }
            },
            "img": "systems/pf2e/icons/default-icons/action.svg",
            "name": "Darkvision",
            "sort": 200000,
            "type": "action"
        },
        {
            "_id": "Kpl3lLjfHM0hWhT5",
            "data": {
                "actionCategory": {
                    "value": "defensive"
                },
                "actionType": {
                    "value": "reaction"
                },
                "actions": {
                    "value": null
                },
                "description": {
                    "value": "<p data-visibility=\"gm\"><strong>Trigger</strong> The wight is reduced to 0 Hit Points.</p>\n<p><strong>Effect</strong> The wight makes a Strike before being destroyed. It doesn't gain any temporary HP from drain life on this Strike.</p>"
                },
                "requirements": {
                    "value": ""
                },
                "rules": [],
                "slug": null,
                "source": {
                    "value": ""
                },
                "traits": {
                    "custom": "",
                    "rarity": {
                        "value": "common"
                    },
                    "value": []
                },
                "trigger": {
                    "value": ""
                },
                "weapon": {
                    "value": ""
                }
            },
            "effects": [],
            "flags": {},
            "img": "systems/pf2e/icons/actions/Reaction.webp",
            "name": "Final Spite",
            "sort": 300000,
            "type": "action"
        },
        {
            "_id": "DfH9lwTe6vTJq3Q6",
            "data": {
                "actionCategory": {
                    "value": "defensive"
                },
                "actionType": {
                    "value": "passive"
                },
                "actions": {
                    "value": null
                },
                "description": {
                    "value": "<p>A creature with negative healing draws health from negative energy rather than positive energy. It is damaged by positive damage and is not healed by positive healing effects. It does not take negative damage, and it is healed by negative effects that heal undead.</p>"
                },
                "requirements": {
                    "value": ""
                },
                "rules": [
                    {
                        "key": "ActiveEffectLike",
                        "mode": "override",
                        "path": "data.attributes.hp.negativeHealing",
                        "value": true
                    }
                ],
                "slug": "negative-healing",
                "source": {
                    "value": ""
                },
                "traits": {
                    "custom": "",
                    "rarity": {
                        "value": "common"
                    },
                    "value": []
                },
                "trigger": {
                    "value": ""
                },
                "weapon": {
                    "value": ""
                }
            },
            "effects": [],
            "flags": {
                "core": {
                    "sourceId": "Compendium.pf2e.bestiary-ability-glossary-srd.TTCw5NusiSSkJU1x"
                }
            },
            "img": "systems/pf2e/icons/default-icons/action.svg",
            "name": "Negative Healing",
            "sort": 400000,
            "type": "action"
        },
        {
            "_id": "v2Ifd6tb1IDRFqFO",
            "data": {
                "actionCategory": {
                    "value": "offensive"
                },
                "actionType": {
                    "value": "passive"
                },
                "actions": {
                    "value": null
                },
                "description": {
                    "value": "<p>When the wight damages a living creature with its claw Strike, <em data-visibility=\"gm\">the wight gains [[/r 3 #temporary Hit Points]] temporary Hit Points and </em>the creature must succeed at a <span data-pf2-check=\"fortitude\" data-pf2-dc=\"17\" data-pf2-traits=\"divine,necromancy\" data-pf2-label=\"Drain Life DC\" data-pf2-show-dc=\"gm\">Fortitude</span> save<em data-visibility=\"gm\"> or become @Compendium[pf2e.conditionitems.Drained]{Drained 1}</em>.</p>\n<p><em data-visibility=\"gm\">Further damage dealt by the wraith increases the amount of drain by 1 on a failed save to a maximum of drained 4</em></p>"
                },
                "requirements": {
                    "value": ""
                },
                "rules": [],
                "slug": null,
                "source": {
                    "value": ""
                },
                "traits": {
                    "custom": "",
                    "rarity": {
                        "value": "common"
                    },
                    "value": [
                        "divine",
                        "necromancy"
                    ]
                },
                "trigger": {
                    "value": ""
                },
                "weapon": {
                    "value": ""
                }
            },
            "effects": [],
            "flags": {},
            "img": "systems/pf2e/icons/actions/Passive.webp",
            "name": "Drain Life",
            "sort": 500000,
            "type": "action"
        },
        {
            "_id": "Umi21NTMrg5JA3IQ",
            "data": {
                "actionCategory": {
                    "value": "offensive"
                },
                "actionType": {
                    "value": "passive"
                },
                "actions": {
                    "value": null
                },
                "description": {
                    "value": "<p>A living humanoid slain by a wight's claw Strike rises as a wight after [[/br 1d4 #rounds]]{1d4 rounds}.</p>\n<p>This wight spawn is under the command of the wight that killed it. It doesn't have Drain Life or Wight Spawn and becomes @Compendium[pf2e.conditionitems.Clumsy]{Clumsy 2} for as long as it is a wight spawn.</p>\n<p>If the creator of the wight spawn dies, the wight spawn becomes a full-fledged, autonomous wight; it regains its free will, gains Drain Life and Wight Spawn, and is no longer clumsy.</p>"
                },
                "requirements": {
                    "value": ""
                },
                "rules": [],
                "slug": null,
                "source": {
                    "value": ""
                },
                "traits": {
                    "custom": "",
                    "rarity": {
                        "value": "common"
                    },
                    "value": [
                        "divine",
                        "necromancy"
                    ]
                },
                "trigger": {
                    "value": ""
                },
                "weapon": {
                    "value": ""
                }
            },
            "effects": [],
            "flags": {},
            "img": "systems/pf2e/icons/actions/Passive.webp",
            "name": "Wight Spawn",
            "sort": 600000,
            "type": "action"
        },
        {
            "_id": "kTAxlq5A4eLqqoED",
            "data": {
                "description": {
                    "value": ""
                },
                "item": {
                    "value": 0
                },
                "mod": {
                    "value": 11
                },
                "proficient": {
                    "value": 0
                },
                "rules": [],
                "slug": null,
                "source": {
                    "value": ""
                },
                "traits": {
                    "custom": "",
                    "rarity": {
                        "value": "common"
                    },
                    "value": []
                }
            },
            "effects": [],
            "flags": {},
            "img": "systems/pf2e/icons/default-icons/lore.svg",
            "name": "Athletics",
            "sort": 700000,
            "type": "lore"
        },
        {
            "_id": "vG08rxUqChNxCzq0",
            "data": {
                "description": {
                    "value": ""
                },
                "item": {
                    "value": 0
                },
                "mod": {
                    "value": 9
                },
                "proficient": {
                    "value": 0
                },
                "rules": [],
                "slug": null,
                "source": {
                    "value": ""
                },
                "traits": {
                    "custom": "",
                    "rarity": {
                        "value": "common"
                    },
                    "value": []
                }
            },
            "effects": [],
            "flags": {},
            "img": "systems/pf2e/icons/default-icons/lore.svg",
            "name": "Intimidation",
            "sort": 800000,
            "type": "lore"
        },
        {
            "_id": "YIVOTaxMUF2efTCa",
            "data": {
                "description": {
                    "value": ""
                },
                "item": {
                    "value": 0
                },
                "mod": {
                    "value": 6
                },
                "proficient": {
                    "value": 0
                },
                "rules": [],
                "slug": null,
                "source": {
                    "value": ""
                },
                "traits": {
                    "custom": "",
                    "rarity": {
                        "value": "common"
                    },
                    "value": []
                }
            },
            "effects": [],
            "flags": {},
            "img": "systems/pf2e/icons/default-icons/lore.svg",
            "name": "Stealth",
            "sort": 900000,
            "type": "lore"
        }
    ],
    "name": "Wight",
    "token": {
        "disposition": -1,
        "height": 1,
        "img": "systems/pf2e/icons/default-icons/npc.svg",
        "name": "Wight",
        "width": 1
    },
    "type": "npc"
}
